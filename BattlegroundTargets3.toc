## Interface: 90001
## Title: BattlegroundTargets3
## Notes: A Friend-Enemy-Unit-Frame for battlegrounds.
## Rework: Чёрнобог-Deathguard
## Version: v06
## SavedVariables: BattlegroundTargets_Options

Locales\BattlegroundTargets-localized-carrier.lua
Locales\BattlegroundTargets-localized-racenames.lua
Locales\BattlegroundTargets-localized-talents.lua
Locales\BattlegroundTargets-localized-transliteration.lua

Locales\BattlegroundTargets-localization-deDE.lua
Locales\BattlegroundTargets-localization-enUS.lua
Locales\BattlegroundTargets-localization-esES.lua
Locales\BattlegroundTargets-localization-esMX.lua
Locales\BattlegroundTargets-localization-frFR.lua
Locales\BattlegroundTargets-localization-itIT.lua
Locales\BattlegroundTargets-localization-koKR.lua
Locales\BattlegroundTargets-localization-ptBR.lua
Locales\BattlegroundTargets-localization-ruRU.lua
Locales\BattlegroundTargets-localization-zhCN.lua
Locales\BattlegroundTargets-localization-zhTW.lua

BattlegroundTargets-texture-button.tga
BattlegroundTargets-texture-icons.tga

utf8.lua

BattlegroundTargets_templates.lua
BattlegroundTargets.lua
